//
//  CellBarChart.swift
//  SalesReport
//
//  Created by user on 27/01/2020.
//  Copyright © 2020 user. All rights reserved.
//

import Foundation
import UIKit
import Charts

class CellBarChart: UITableViewCell {
    
    @IBOutlet weak var barchart: BarChartView!
    @IBOutlet weak var lblName: UILabel!
    
}
