//
//  CellLineChart.swift
//  SalesReport
//
//  Created by user on 02/02/2020.
//  Copyright © 2020 user. All rights reserved.
//


import Foundation
import UIKit
import Charts

class CellLineChart: UITableViewCell {
    
    @IBOutlet weak var lineChartView: LineChartView!
    
}
