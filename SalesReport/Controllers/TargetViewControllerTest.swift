//
//  TargetViewControllerTest.swift
//  SalesReport
//
//  Created by user on 05/02/2020.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit
import Charts
import Alamofire

class TargetViewControllerTest: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var viewLineChart: LineChartView!
    var numbers : [Double] = []
    
    @IBOutlet weak var vieBarChart: BarChartView!
    
    var spaceBar : Double = 0.2
    var barSpacing : Double = 0.00
    let months = ["Jan", "Feb", "Mar", "Apr", "May"]
    let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0]
    let unitsBought = [10.0, 14.0, 60.0, 13.0, 2.0]
    
    @IBOutlet weak var filterByStack: UIStackView!
    @IBOutlet weak var filterLbl: UILabel!
    
    override open func viewDidLoad()
    {
            self.title = "Target"
            super.viewDidLoad()
            
            numbers.append(10) //here we add the data to the array.
            numbers.append(1)
            numbers.append(5)
            numbers.append(20)
            numbers.append(100)
            numbers.append(10) //here we add the data to the array.
            numbers.append(1)
            numbers.append(5)
            numbers.append(20)
            numbers.append(100)
            numbers.append(20)
            numbers.append(100)
            
            //updateBarChart()
            
            tableView.delegate = self
            tableView.dataSource = self
            
            
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = UIImage(named:"filter")
        //Set bound to reposition
        let imageOffsetY:CGFloat = 0.0;
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        //Add image to mutable string
        completeText.append(attachmentString)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: "    Filter")
        completeText.append(textAfterIcon)
        self.filterLbl.textAlignment = .center;
        self.filterLbl.attributedText = completeText;
        
        
        var color2 = UIColor(netHex:0x2962ff)
        self.filterByStack.addBackground(color: color2)
            //self.filterstack.spacing = 2
        apiHandler()
    }
    
    func apiHandler () {
        let parameters: Parameters = ["year": "",
                                      "month": "",
                                      "fromdate": "",
                                      "todate": "",
                                      "location": "",
                                      "sortdesc": "",
                                      "sortass": "",
                                      "username": "sshah1"]
        let URL:String = "http://lp00491:8000/target-mobile-api"
        Alamofire.request(URL, method: .post, parameters: parameters)
                    .validate()
                    .responseJSON { response in
        // 3 - HTTP response handle
                    guard response.result.isSuccess else {
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    return
                        
                 print(response)
                        
                }
        }
    }
    
    override public func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          print("FirstViewController will appear")
      }

      override public func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          print("FirstViewController will disappear")
      }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
        
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CellBarChart = self.tableView.dequeueReusableCell(withIdentifier: "cellTarget") as! CellBarChart
        
        updateBarChart(barChartView: cell.barchart)
        
        return cell
    }
    
    var stackLabels: [String] = [""]

    func updateBarChart(barChartView : BarChartView){
            
        // Do any additional setup after loading the view, typically from a nib.
                   //barChartView.delegate = self
                   barChartView.noDataText = "You need to provide data for the chart."
                   barChartView.chartDescription?.text = ""


                   //legend
                   let legend = barChartView.legend
                   legend.enabled = true
                   legend.horizontalAlignment = .right
                   legend.verticalAlignment = .top
                   legend.orientation = .vertical
                   legend.drawInside = true
                   legend.yOffset = 10.0;
                   legend.xOffset = 10.0;
                   legend.yEntrySpace = 0.0;


                   let xaxis = barChartView.xAxis
                   xaxis.drawGridLinesEnabled = true
                   xaxis.labelPosition = .bottom
                   xaxis.centerAxisLabelsEnabled = true
                   xaxis.valueFormatter = IndexAxisValueFormatter(values:self.months)
                   xaxis.granularity = 1


                   let leftAxisFormatter = NumberFormatter()
                   leftAxisFormatter.maximumFractionDigits = 1

                   let yaxis = barChartView.leftAxis
                   yaxis.spaceTop = 0.35
                   yaxis.axisMinimum = 0
                   yaxis.drawGridLinesEnabled = false

                   barChartView.rightAxis.enabled = false
                  //axisFormatDelegate = self

                   setChart(barChartView: barChartView)
    }

    func setChart(barChartView : BarChartView) {
               barChartView.noDataText = "You need to provide data for the chart."
               var dataEntries: [BarChartDataEntry] = []
               var dataEntries1: [BarChartDataEntry] = []

               for i in 0..<self.months.count {

                   let dataEntry = BarChartDataEntry(x: Double(i) , y: self.unitsSold[i])
                   dataEntries.append(dataEntry)

                   let dataEntry1 = BarChartDataEntry(x: Double(i) , y: self.self.unitsBought[i])
                   dataEntries1.append(dataEntry1)

                   //stack barchart
                   //let dataEntry = BarChartDataEntry(x: Double(i), yValues:  [self.unitsSold[i],self.unitsBought[i]], label: "groupChart")
               }
        
            let chartDataSet = BarChartDataSet(entries: dataEntries, label: "Unit sold")
            let chartDataSet1 = BarChartDataSet(entries: dataEntries1, label: "Unit Bought")

               let dataSets: [BarChartDataSet] = [chartDataSet,chartDataSet1]
               chartDataSet.colors = [UIColor(red: 230/255, green: 126/255, blue: 34/255, alpha: 1)]
               //chartDataSet.colors = ChartColorTemplates.colorful()
               //let chartData = BarChartData(dataSet: chartDataSet)

               let chartData = BarChartData(dataSets: dataSets)

               let groupSpace = 0.3
               let barSpace = 0.05
               let barWidth = 0.3
               // (0.3 + 0.05) * 2 + 0.3 = 1.00 -> interval per "group"

               let groupCount = self.months.count
               let startYear = 0

               chartData.barWidth = barWidth;
               barChartView.xAxis.axisMinimum = Double(startYear)
               let gg = chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
               print("Groupspace: \(gg)")
               barChartView.xAxis.axisMaximum = Double(startYear) + gg * Double(groupCount)

               chartData.groupBars(fromX: Double(startYear), groupSpace: groupSpace, barSpace: barSpace)
               //chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
               barChartView.notifyDataSetChanged()

               barChartView.data = chartData

               //background color
               barChartView.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)

               //chart animation
               //barChartView.animate(xAxisDuration: 1.5, yAxisDuration: 1.5, easingOption: .linear)

    }

        
}

