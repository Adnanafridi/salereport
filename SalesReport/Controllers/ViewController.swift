//
//  ViewController.swift
//  SalesReport
//
//  Created by user on 26/01/2020.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit
import Charts
import Alamofire
import SwiftyJSON
import DropDown
import ScrollingContentViewController

class ViewController: UIViewController ,MyDelegate , UIPopoverPresentationControllerDelegate {
    
    func filterCallBack(year: String, month: String, fromDate: String, toDate: String, location: String, product: String, salesman: String, day: String) {
       apiHandler(yearApi: year,monthApi: month,fromDateApi: fromDate,toDateApi: toDate,locationApi: location)
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortByStack: UIStackView!
    
    @IBOutlet weak var filterstack: UIStackView!
    
    @IBOutlet weak var filterLbl: UILabel!
    @IBOutlet weak var ivsort: UIImageView!
    @IBOutlet weak var sortBydropdownIV: UIImageView!
    @IBOutlet weak var sortByLbl: UILabel!
    
    let dropDown = DropDown()
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var svMainContainer: UIScrollView!
    
    @IBOutlet weak var chartQuantity: LineChartView!
    @IBOutlet weak var chartTotalQuantity: LineChartView!

    @IBOutlet weak var chartTotalRevenue: LineChartView!
    @IBOutlet weak var chartRevenue: LineChartView!
    @IBOutlet weak var chartUnitsQuantity: LineChartView!
    @IBOutlet weak var chartUnitsRevenue: LineChartView!

    @IBOutlet weak var chartAccessoriesQuantity: LineChartView!
    
    @IBOutlet weak var chartAccessoriesRevenue: LineChartView!
    
    @IBOutlet weak var chartSpareQuantity: LineChartView!
    @IBOutlet weak var chartSpareRevenue: LineChartView!
    
    
    var quantityArrayChart = Dictionary<String, Any>()
    var totalQuantityArrayChart = Dictionary<String, Any>()
    var totalRevenueArrayChart = Dictionary<String, Any>()
    var revenueArrayChart = Dictionary<String, Any>()
    var unitsQuantityArrayChart = Dictionary<String, Any>()
    var unitsRevenueArrayChart = Dictionary<String, Any>()
    var accessoriesQuantityArrayChart = Dictionary<String, Any>()
    var accessoriesRevenueArrayChart = Dictionary<String, Any>()
    var spareQuantityArrayChart = Dictionary<String, Any>()
    var spareRevenueArrayChart = Dictionary<String, Any>()
    
    
    var yearGloble : String = ""
    var monthGloble : String = ""
    var fromDateGloble : String = ""
    var toDateGloble : String = ""
    var locationGloble : String = ""
    var vSpinner : UIView?
    
    
    override open func viewDidLoad()
    {
        super.viewDidLoad()
        
        //contentView.translatesAutoresizingMaskIntoConstraints = false
        //svMainContainer.contentSize = CGSize(width: svMainContainer.contentSize.width, height: 4800)
        //contentView.heightAnchor.constraint(equalToConstant: 4800).isActive = true
        //contentView.widthAnchor.constraint(equalTo: svMainContainer.widthAnchor).isActive = true
//        NSLayoutConstraint.activate([
//          contentView.leadingAnchor.constraint(equalTo: svMainContainer.leadingAnchor),
//          contentView.trailingAnchor.constraint(equalTo: svMainContainer.trailingAnchor),
//          contentView.topAnchor.constraint(equalTo: svMainContainer.topAnchor),
//          contentView.bottomAnchor.constraint(equalTo: svMainContainer.bottomAnchor)
//        ])

        //contentView.addSubview(svMainContainer)
        //svMainContainer.addSubview(contentView)
        
        
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = UIImage(named:"filter")
        //Set bound to reposition
        let imageOffsetY:CGFloat = 0.0;
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        //Add image to mutable string
        completeText.append(attachmentString)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: "    Filter")
        completeText.append(textAfterIcon)
        self.filterLbl.textAlignment = .center;
        self.filterLbl.attributedText = completeText;
        
        var color2 = UIColor(netHex:0x2962ff)
        self.sortByStack.addBackground(color: color2)
            //self.sortByStack.spacing = 2
        
        self.filterstack.addBackground(color: color2)
        
            
            //updateBarChart()
            //tableView.delegate = self
            //tableView.dataSource = self
        
        
        ivsort.image = UIImage(named:"sort_dec")
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        ivsort.isUserInteractionEnabled = true
        ivsort.addGestureRecognizer(tapGestureRecognizer)
        
        
        let tapGesturesortTapped = UITapGestureRecognizer(target: self, action: #selector(sortTapped(tapGestureRecognizer:)))
        sortBydropdownIV.isUserInteractionEnabled = true
        sortBydropdownIV.addGestureRecognizer(tapGesturesortTapped)
        
        
        let tapGesturesort = UITapGestureRecognizer(target: self, action: #selector(sortTapped(tapGestureRecognizer:)))
        sortByLbl.isUserInteractionEnabled = true
        sortByLbl.addGestureRecognizer(tapGesturesort)
        
        let tapGestureFilter = UITapGestureRecognizer(target: self, action: #selector(filterTapped(tapGestureRecognizer:)))
        filterLbl.isUserInteractionEnabled = true
        filterLbl.addGestureRecognizer(tapGestureFilter)
        
        //imageName = "sort_dec.png"
        //var imageView : UIImageView
        //imageView  = UIImageView(frame:CGRectMake(10, 50, 100, 300));
        //self.view.addSubview(imageView)
        apiHandler(yearApi: "",monthApi: "",fromDateApi: "2019-08-01",toDateApi: User.sharedInstance.currentDate(),locationApi: "")
        
        dropdownSort()
        apiLocationHandler()
        apiProductHandler()
        apiSaleManHandler()
        //apiSortingHandler()
        apiYearHandler()
        
    }
    
    override func viewWillLayoutSubviews(){
        super.viewWillLayoutSubviews()
        
    }
    
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        print("")
    }

    
    @objc func filterTapped(tapGestureRecognizer: UITapGestureRecognizer){
        popupFilterCall()
     }
    
    public func popupFilterCall(){
           let vc = storyboard?.instantiateViewController(withIdentifier: "PopupFilterController") as? FilterPopUp        
            vc?.delegate = self
            vc?.callFrom = "Summary"
            vc?.fromDate = fromDateGloble
            vc?.toDate = toDateGloble
            vc?.year = yearGloble
            vc?.month = monthGloble
            vc?.location = locationGloble
            self.present(vc!, animated: true, completion: nil)
    }
       
    
    @objc func sortTapped(tapGestureRecognizer: UITapGestureRecognizer){
        dropDown.show()
     }
    
    func dropdownSort() {

        // The view to which the drop down will appear on
        dropDown.anchorView = view // UIView or UIBarButtonItem
        
        dropDown.dataSource = User.sharedInstance.sortBy
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.dropDown.hide()
            if item == "Remove Sort By"{
                self.sortByLbl.text = "Sort By"
            }
            else {
                self.sortByLbl.text = item
            }
            self.apiHandler(yearApi: self.yearGloble,monthApi: self.monthGloble,fromDateApi: self.fromDateGloble,toDateApi: self.toDateGloble,locationApi: self.locationGloble)
        }
        dropDown.bottomOffset = CGPoint(x: (dropDown.anchorView?.plainView.bounds.width)!/10, y:(dropDown.anchorView?.plainView.bounds.width)!/2.7)
        //dropDown.center = CGPoint(x: -(dropDown.anchorView?.plainView.bounds.height)!, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.width = 200
        dropDown.direction = .any
        DropDown.appearance().cornerRadius = 10
        
    }

    
    func apiHandler (yearApi : String , monthApi : String , fromDateApi : String , toDateApi : String , locationApi : String ) {
        showSpinner(onView: self.view!)
        
        var yearParam : String = ""
        var monthParam : String = ""
        var fromDateParam : String = ""
        var toDateParam : String = ""
        var locationParam : String = ""
        var sortByAss : String = ""
        var sortByDec : String = ""
        
        yearParam = yearApi;
        monthParam=monthApi;
        fromDateParam=fromDateApi;
        toDateParam=toDateApi
        locationParam=locationApi
        
        
        yearGloble = yearApi;
        monthGloble=monthApi;
        fromDateGloble=fromDateApi;
        toDateGloble=toDateApi
        locationGloble=locationApi
        
        
        if yearApi != ""{
            yearParam = "'"+yearApi+"'"
        }
        
        if monthApi != ""{
            monthParam = "'"+monthApi+"'"
        }
        
        if fromDateApi != ""{
            fromDateParam = ""+fromDateApi+""
        }
        
        if toDateApi != ""{
            toDateParam = ""+toDateApi+""
        }
        
        if locationApi != ""{
            locationParam = "'"+locationApi+"'"
        }
        
        if self.sortByLbl.text == "Sort By"{
            sortByAss = ""
            sortByDec = ""
        } else if self.sortByLbl.text == "Quantity"{
            if sortBt == "asc" {
                sortByAss = "qty"
                sortByDec = ""
            }else {
                
                sortByAss = ""
                sortByDec = "qty"
            }
            
        }else {
            if sortBt == "asc" {
               sortByAss = "revenue"
               sortByDec = ""
            }else {
                sortByAss = ""
                sortByDec = "revenue"
            }
        }
        
        //["Quantity" , "Revenue"]
        
        let parameters: Parameters = ["year": yearParam,
                                      "month": monthParam,
                                      "fromdate": fromDateParam,
                                      "todate": toDateParam,
                                      "location": locationParam,
                                      "sortdesc": sortByDec,
                                      "sortass": sortByAss,
                                      "username": "sshah1"]
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-HTTP-Method-Override": "PATCH"
        ]
        //let postData = NSMutableData(data:parameters.data(using: String.Encoding.utf8)!)
        //let responseString = String(data: parameters, encoding: .utf8)
        let URL:String = "http://192.168.18.115:8000/summary-mobile-api"
        Alamofire.request(URL, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers:headers)
                    .validate()
                    .responseJSON { response in
                        self.removeSpinner()
                        guard response.result.isSuccess else {
                        //print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                            let alertController = UIAlertController(title: "Alert", message:
                                "No Record Available", preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                            self.present(alertController, animated: true, completion: nil)
                            self.svMainContainer.isHidden = true
                        return
                    }
                self.svMainContainer.isHidden = false
                let swiftyJsonVar = JSON(response.result.value!)
                if let resData = swiftyJsonVar["quantity"].dictionaryObject {
                    self.quantityArrayChart  = resData  as! Dictionary<String, Any>
                }
                if let resData = swiftyJsonVar["totalqty"].dictionaryObject {
                    self.totalQuantityArrayChart  = resData  as! Dictionary<String, Any>
                }
                if let resData = swiftyJsonVar["totalrev"].dictionaryObject {
                    self.totalRevenueArrayChart  = resData  as! Dictionary<String, Any>
                }
                if let resData = swiftyJsonVar["revenue"].dictionaryObject {
                    self.revenueArrayChart  = resData  as! Dictionary<String, Any>
                }
                if let resData = swiftyJsonVar["unitsqty"].dictionaryObject {
                    self.unitsQuantityArrayChart  = resData  as! Dictionary<String, Any>
                }
                if let resData = swiftyJsonVar["unitsrev"].dictionaryObject {
                    self.unitsRevenueArrayChart  = resData  as! Dictionary<String, Any>
                }
                if let resData = swiftyJsonVar["accessoriesqty"].dictionaryObject {
                    self.accessoriesQuantityArrayChart  = resData  as! Dictionary<String, Any>
                }
                if let resData = swiftyJsonVar["accessoriesrev"].dictionaryObject {
                    self.accessoriesRevenueArrayChart  = resData  as! Dictionary<String, Any>
                }
                if let resData = swiftyJsonVar["sparesqty"].dictionaryObject {
                    self.spareQuantityArrayChart  = resData  as! Dictionary<String, Any>
                }
                if let resData = swiftyJsonVar["sparesrev"].dictionaryObject {
                    self.spareRevenueArrayChart  = resData  as! Dictionary<String, Any>
                }
                self.updateGraphQuantity(quantityArray: self.quantityArrayChart,lineChart: self.chartQuantity)
                
                self.updateGraphQuantity(quantityArray: self.unitsQuantityArrayChart,lineChart: self.chartUnitsQuantity)
                self.updateGraphQuantity(quantityArray: self.accessoriesQuantityArrayChart,lineChart: self.chartAccessoriesQuantity)
                self.updateGraphQuantity(quantityArray: self.spareQuantityArrayChart,lineChart: self.chartSpareQuantity)
                
                
                self.updateGraphTotalQuantity(quantityTotalArray: self.totalQuantityArrayChart,lineChart: self.chartTotalQuantity)
                self.updateGraphTotalQuantity(quantityTotalArray: self.totalRevenueArrayChart,lineChart: self.chartTotalRevenue)
                
                
                self.updateGraphRevenue(quantityTotalArra: self.revenueArrayChart,lineChart: self.chartRevenue, name: "Revenue")
                self.updateGraphRevenue(quantityTotalArra: self.unitsRevenueArrayChart,lineChart: self.chartUnitsRevenue, name: "Units Revenue")
                self.updateGraphRevenue(quantityTotalArra: self.accessoriesRevenueArrayChart,lineChart: self.chartAccessoriesRevenue,name: "Accessories Revenue")
                self.updateGraphRevenue(quantityTotalArra: self.spareRevenueArrayChart,lineChart: self.chartSpareRevenue,name: "Spare Revenue")
        }
    }
    
    var sortBt : String = "desc"
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        if self.sortByLbl.text == "Sort By"{
            let alertController = UIAlertController(title: "Alert", message:
                "Please select Sort By", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        if sortBt == "desc" {
            sortBt = "asc"
            ivsort.image = UIImage(named:"sort_assending")
        }else {
            sortBt = "desc"
            ivsort.image = UIImage(named:"sort_dec")
        }
        
        self.apiHandler(yearApi: yearGloble,monthApi: monthGloble,fromDateApi: fromDateGloble,toDateApi: toDateGloble,locationApi: locationGloble)
        
    }
        
    
    override public func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          print("FirstViewController will disappear")
    }
     
      
    func updateGraphRevenue(quantityTotalArra : Dictionary<String, Any> ,lineChart : LineChartView , name : String) {
        
        let data = LineChartData()
        var quantitydatasets =  quantityTotalArra as! NSDictionary
          for index in 0...1 {
              var lineChartEntry  = [ChartDataEntry]() //this is the Array that will eventually be displayed on the graph.
            
            var quantitydata :  [Double] = []
            
            if index == 0 {
                quantitydata = quantitydatasets["revenue"] as! [Double]
            }else {
                quantitydata = quantitydatasets["gp"] as! [Double]
            }
            
              for i in 0..<quantitydata.count{
                     //var dataEntryFirstString =  quantitydata as! [Double]
                     let value = ChartDataEntry(x: Double(i), y: quantitydata[i]) // here we set the X and Y status in a data chart entry
                     lineChartEntry.append(value) // here we add it to the data set
              }
            var line1 = LineChartDataSet(entries: lineChartEntry, label: name) //Here we convert lineChartEntry to a LineChartDataSet

            if index == 1 {
                line1 = LineChartDataSet(entries: lineChartEntry, label: "GP") //Here we convert lineChartEntry to a LineChartDataSet
            }
              //line1.drawCirclesEnabled = false
              line1.circleRadius = 3
              
              //var arrayColor : [UIColor] = []
              //var color : UIColor!
              
              //var colorCode = self.chartPlacesArray[position]["colors"][cout]  as? String ?? ""
              
              //color = UIColor.init(hex: quantityTotalArra[index]["borderColor"] as? String ?? ""+"ff")//UIColor("#000000ff")
              
              //arrayColor.append(color)
              if index == 0 {
                  var colorCode =  "#0000FF"
                  line1.colors = [UIColor.init(hex: colorCode+"ff")!] //Sets the colour to blue
                  line1.circleColors = [UIColor.init(hex: colorCode+"ff")!]
              }else if index == 1 {
                 var colorCode = "#FFA500"
                 line1.colors = [UIColor.init(hex: colorCode+"ff")!] //Sets the colour to blue
                 line1.circleColors = [UIColor.init(hex: colorCode+"ff")!]
              }
              //line1.colors = arrayColor //Sets the colour to blue
              //This is the object that will be added to the chart
              data.addDataSet(line1) //Adds the line to the dataSet
          }
            
            User.sharedInstance.xaxisValues  = quantitydatasets["labels"] as! [String]
            print(User.sharedInstance.xaxisValues)
              
              let xAxis = lineChart.xAxis
              xAxis.labelPosition = .bottom
              xAxis.labelFont = .systemFont(ofSize: 10)
              xAxis.granularity = 1
              xAxis.labelCount = 7
              xAxis.valueFormatter = IndexAxisValueFormatter(values: User.sharedInstance.xaxisValues)
              
              
              lineChart.data = data //finally - it adds the chart data to the chart and causes an update
              lineChart.chartDescription?.text = "" // Here we set the description for the graph
              lineChart.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)

      }
      
      
        
    func updateGraphTotalQuantity(quantityTotalArray : Dictionary<String, Any> ,lineChart : LineChartView) {
        //this is the Array that will eventually be displayed on the graph.

        var quantitydatasets =  quantityTotalArray["datasets"] as! [NSDictionary]
        
        let data = LineChartData()
        
        var arrayColor : [UIColor] = []
        var color : UIColor!

        
        for cout in 0..<quantitydatasets.count {
                           //var color = "#000000"
            var color : UIColor!
                           
            var colorCode = quantitydatasets[cout]["borderColor"] as? String ?? ""
                           
            color = UIColor.init(hex: colorCode+"ff")//UIColor("#000000ff")
                           
            arrayColor.append(color)
        }
        
        for index in 0..<(quantitydatasets as AnyObject).count {
            var lineChartEntry  = [ChartDataEntry]()
            for i in 0..<(quantitydatasets[index]["data"]! as AnyObject).count{
                   var dataEntryFirstString =  quantitydatasets[index]["data"] as! [Double]
                   let value = ChartDataEntry(x: Double(i), y: dataEntryFirstString[i]) // here we set the X and Y status in a data chart entry
                   lineChartEntry.append(value) // here we add it to the data set
            }
            //lineChartEntry.sort(by: { $0.x < $1.x })
            
            let line1 = LineChartDataSet(entries: lineChartEntry, label: quantitydatasets[index]["label"] as? String ?? "") //Here we convert lineChartEntry to a LineChartDataSet
            //line1.drawCirclesEnabled = false
            line1.circleRadius = 3
            
            if index == 0 {
                var colorCode = quantitydatasets[index]["borderColor"] as? String ?? ""
                line1.colors = [UIColor.init(hex: colorCode+"ff")!] //Sets the colour to blue
                line1.circleColors = [UIColor.init(hex: colorCode+"ff")!]
            }else if index == 1 {
               var colorCode = quantitydatasets[index]["borderColor"] as? String ?? ""
               line1.colors = [UIColor.init(hex: colorCode+"ff")!] //Sets the colour to blue
               line1.circleColors = [UIColor.init(hex: colorCode+"ff")!]
            }else if index == 2 {
               var colorCode = quantitydatasets[index]["borderColor"] as? String ?? ""
               line1.colors = [UIColor.init(hex: colorCode+"ff")!] //Sets the colour to blue
               line1.circleColors = [UIColor.init(hex: colorCode+"ff")!]
            }
            //line1.color(atIndex: arrayColor[index]) //Sets the colour to blue
            //line1.color(atIndex: <#T##Int#>)
            //This is the object that will be added to the chart
            data.addDataSet(line1) //Adds the line to the dataSet
        }
            //data.sort(by: { $0.x < $1.x })
            User.sharedInstance.xaxisValues  = quantityTotalArray["labels"] as! [String]
            print(User.sharedInstance.xaxisValues)
            
            let xAxis = lineChart.xAxis
            xAxis.labelPosition = .bottom
            xAxis.labelFont = .systemFont(ofSize: 10)
            xAxis.granularity = 1
            xAxis.labelCount = 7
            xAxis.valueFormatter = IndexAxisValueFormatter(values: User.sharedInstance.xaxisValues)
            
            
            lineChart.data = data //finally - it adds the chart data to the chart and causes an update
            lineChart.chartDescription?.text = "" // Here we set the description for the graph
            lineChart.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)

    }
    
    
      func updateGraphQuantity(quantityArray : Dictionary<String, Any> ,lineChart : LineChartView) {
                
              let data = LineChartData()
              
              var lineChartEntry  = [ChartDataEntry]() //this is the Array that will eventually be displayed on the graph.
              
              //here is the for loop
              for i in 0..<(quantityArray["data"]! as AnyObject).count{
                     var dataEntryFirstString =  quantityArray["data"] as! [Double]
                     let value = ChartDataEntry(x: Double(i), y: dataEntryFirstString[i]) // here we set the X and Y status in a data chart entry
                     lineChartEntry.append(value) // here we add it to the data set
              }
            
              let line1 = LineChartDataSet(entries: lineChartEntry, label: "Quantity") //Here we convert lineChartEntry to a LineChartDataSet
              //line1.colors = [NSUIColor.blue] //Sets the colour to blue
              //This is the object that will be added to the chart
            //line1.drawCirclesEnabled = false
            line1.circleRadius = 3
            data.addDataSet(line1) //Adds the line to the dataSet

              User.sharedInstance.xaxisValues  = quantityArray["labels"] as! [String]
              print(User.sharedInstance.xaxisValues)
              
              let xAxis = lineChart.xAxis
              xAxis.labelPosition = .bottom
              xAxis.labelFont = .systemFont(ofSize: 10)
              xAxis.granularity = 1
              xAxis.labelCount = 7
              xAxis.valueFormatter = IndexAxisValueFormatter(values: User.sharedInstance.xaxisValues)
                
              
              
              lineChart.data = data //finally - it adds the chart data to the chart and causes an update
              lineChart.chartDescription?.text = "" // Here we set the description for the graph
              lineChart.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)

      }
      
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.popoverPresentationController?.delegate = self
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
         //numbers.append(100)
    }
    
    
    func apiLocationHandler () {

        let URL:String = "http://192.168.18.115:8000/ginvsaleLocationLOV"
        Alamofire.request(URL, method: .get, parameters: nil)
                    .validate()
                    .responseJSON { response in
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                        return
                    }
                    let swiftyJsonVar = JSON(response.result.value!)
                        if let resData = swiftyJsonVar["data"].arrayObject {
                            User.sharedInstance.locationArray  = resData  as! [Dictionary<String, Any>]
                    }
                    var locationValues : [String] = []
                    var counterData = User.sharedInstance.locationArray.count
                    for index in 0..<counterData{
                        locationValues.append(User.sharedInstance.locationArray[index]["sale_location"] as! String)
                    }
                   User.sharedInstance.locationaData = locationValues
        }
        
    }
    
    
    func apiSortingHandler () {

        let URL:String = "http://192.168.18.115:8000/ginvSortingLOV"
        Alamofire.request(URL, method: .get, parameters: nil)
                    .validate()
                    .responseJSON { response in
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                        return
                    }
                        let swiftyJsonVar = JSON(response.result.value!)
                        if let resData = swiftyJsonVar["data"].arrayObject {
                            User.sharedInstance.sortArray  = resData  as! [Dictionary<String, Any>]
                        }
        }
        
    }
    
    func apiProductHandler () {

        let URL:String = "http://192.168.18.115:8000/ginvProductLOV"
        Alamofire.request(URL, method: .get, parameters: nil)
                    .validate()
                    .responseJSON { response in
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                        return
                    }
                    let swiftyJsonVar = JSON(response.result.value!)
                    if let resData = swiftyJsonVar["data"].arrayObject {
                        User.sharedInstance.placesArray  = resData  as! [Dictionary<String, Any>]
                    }
                    var productValues : [String] = []
                    var counterData = User.sharedInstance.placesArray.count
                    for index in 0..<counterData{
                         productValues.append(User.sharedInstance.placesArray[index]["item_code"] as! String)
                     }
                    User.sharedInstance.productData = productValues
        }
        
    }
    
    func apiYearHandler () {

        let URL:String = "http://192.168.18.115:8000/ginvsaleYearLOV"
        Alamofire.request(URL, method: .get, parameters: nil)
                    .validate()
                    .responseJSON { response in
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                        return
                    }
                    let swiftyJsonVar = JSON(response.result.value!)
                    if let resData = swiftyJsonVar["data"].arrayObject {
                        User.sharedInstance.yearArray  = resData  as! [Dictionary<String, Any>]
                    }
                    var yearValues : [String] = []
                    var counterData = User.sharedInstance.yearArray.count
                    
                    for index in 0..<counterData{
                        yearValues.append(User.sharedInstance.yearArray[index]["sale_year"] as! String)
                    }
                    
                    User.sharedInstance.yearData = yearValues
                        
        }
        
    }
    
    func apiSaleManHandler () {
        
        let URL:String = "http://192.168.18.115:8000/ginvSaleManLOV"
        Alamofire.request(URL, method: .get, parameters: nil)
                    .validate()
                    .responseJSON { response in
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                        return
                    }
                    
                    let swiftyJsonVar = JSON(response.result.value!)
                    
                    if let resData = swiftyJsonVar["data"].arrayObject {
                        User.sharedInstance.salesmanArray  = resData  as! [Dictionary<String, Any>]
                    }
                    
                    var salesmanValues : [String] = []
                    var counterData = User.sharedInstance.salesmanArray.count
                    
                    for index in 0..<counterData{
                        salesmanValues.append(User.sharedInstance.salesmanArray[index]["salesman"] as! String)
                    }
                    
                    User.sharedInstance.salesmanData = salesmanValues
        }
        
    }

    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
}

    extension UIColor {
        public convenience init?(hex: String) {
            let r, g, b, a: CGFloat

            if hex.hasPrefix("#") {
                let start = hex.index(hex.startIndex, offsetBy: 1)
                let hexColor = String(hex[start...])

                if hexColor.count == 8 {
                    let scanner = Scanner(string: hexColor)
                    var hexNumber: UInt64 = 0

                    if scanner.scanHexInt64(&hexNumber) {
                        r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                        g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                        b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                        a = CGFloat(hexNumber & 0x000000ff) / 255

                        self.init(red: r, green: g, blue: b, alpha: a)
                        return
                    }
                }
            }

            return nil
        }
        
    }

extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}

// 1: Filter sort
// 2: Api handling
//data": [
//{
//"salesman": "Abegail Madrio Apat"
//},
