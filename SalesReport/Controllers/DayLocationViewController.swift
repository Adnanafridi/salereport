//
//  SecondViewController.swift
//  SalesReport
//
//  Created by user on 02/02/2020.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit
import Charts
import Alamofire
import SwiftyJSON
import DropDown

class DayLocationViewController: UIViewController , MyDelegate{

    func filterCallBack(year: String, month: String, fromDate: String, toDate: String, location: String, product: String, salesman: String, day: String) {
        apiHandler(yearApi: year,monthApi: month,fromDateApi: fromDate,toDateApi: toDate,locationApi: location,dayApi: day)
    }
    
    @IBOutlet weak var barChart: BarChartView!
    var chartPlacesArray =  Dictionary<String, Any>()

    @IBOutlet weak var filterByStack: UIStackView!
    @IBOutlet weak var filterLbl: UILabel!
    var stackLabels: [String] = []
    let dropDown = DropDown()
    
    var yearGloble : String = ""
    var monthGloble : String = ""
    var fromDateGloble : String = ""
    var toDateGloble : String = ""
    var locationGloble : String = ""
    var dayGloble : String = ""
    
    override open func viewDidLoad()
    {
            super.viewDidLoad()
            self.title = "Day and Location"
            
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = UIImage(named:"filter")
        //Set bound to reposition
        let imageOffsetY:CGFloat = 0.0;
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        //Add image to mutable string
        completeText.append(attachmentString)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: "    Filter")
        completeText.append(textAfterIcon)
        self.filterLbl.textAlignment = .center;
        self.filterLbl.attributedText = completeText;
        
        var color2 = UIColor(netHex:0x2962ff)
        self.filterByStack.addBackground(color: color2)

        apiHandler(yearApi: "",monthApi: "",fromDateApi: "2019-08-01",toDateApi: User.sharedInstance.currentDate(),locationApi: "",dayApi: dayGloble)
        
        
        let tapGestureFilter = UITapGestureRecognizer(target: self, action: #selector(filterTapped(tapGestureRecognizer:)))
        filterLbl.isUserInteractionEnabled = true
        filterLbl.addGestureRecognizer(tapGestureFilter)
        
    }
    
    
       
    @objc func filterTapped(tapGestureRecognizer: UITapGestureRecognizer){
           popupFilterCall()
    }
       
    public func popupFilterCall(){
        let vc = storyboard?.instantiateViewController(withIdentifier: "PopupFilterDayLocationController") as? FilterPopUpDayLocation
               vc?.delegate = self
               vc?.callFrom = "daylocation"
        vc?.fromDate = fromDateGloble
        vc?.toDate = toDateGloble
        vc?.year = yearGloble
        vc?.month = monthGloble
        vc?.location = locationGloble
        vc?.day = dayGloble
        self.present(vc!, animated: true, completion: nil)
    }
          
       
    
    func apiHandler (yearApi : String , monthApi : String , fromDateApi : String , toDateApi : String , locationApi : String ,dayApi : String) {
    
    showSpinner(onView: self.view!)
    
    var yearParam : String = ""
    var monthParam : String = ""
    var fromDateParam : String = ""
    var toDateParam : String = ""
    var locationParam : String = ""
    var dayParam : String = ""
        
    var sortByAss : String = ""
    var sortByDec : String = ""
    
    yearParam = yearApi;
    monthParam=monthApi;
    fromDateParam=fromDateApi;
    toDateParam=toDateApi
    locationParam=locationApi
    dayParam = dayApi
    
    yearGloble = yearApi;
    monthGloble=monthApi;
    fromDateGloble=fromDateApi;
    toDateGloble=toDateApi
    locationGloble=locationApi
    dayGloble = dayApi
    
    if yearApi != ""{
        yearParam = "'"+yearApi+"'"
    }
    
    if monthApi != ""{
        monthParam = "'"+monthApi+"'"
    }
    
    if fromDateApi != ""{
        fromDateParam = ""+fromDateApi+""
    }
    
    if toDateApi != ""{
        toDateParam = ""+toDateApi+""
    }
    
    if locationApi != ""{
        locationParam = "'"+locationApi+"'"
    }
    
    if dayParam != ""{
        dayParam = ""+dayApi+""
    }
    
    let parameters: Parameters = ["year": yearParam,
                                  "month": monthParam,
                                  "fromdate": fromDateParam,
                                  "todate": toDateParam,
                                  "location": locationParam,
                                  "day": dayParam,
                                  "username": "sshah1"]
        let headers: HTTPHeaders = [
        "Content-Type": "application/json",
        "X-HTTP-Method-Override": "PATCH"
        ]
        let URL:String = "http://192.168.18.115:8000/daylocation-mobile-api"
        Alamofire.request(URL, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers:headers)
                    .validate()
                    .responseJSON { response in
                    // 3 - HTTP response handle
                    self.removeSpinner()
                    guard response.result.isSuccess else {
                    
                    self.barChart.isHidden = true
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    let alertController = UIAlertController(title: "Alert", message:
                        "No Record Available", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                    self.present(alertController, animated: true, completion: nil)
                    return
                    }
                    self.barChart.isHidden = false
                    let swiftyJsonVar = JSON(response.result.value!)
                    if let resData = swiftyJsonVar["dayloc"].dictionaryObject {
                        self.chartPlacesArray  = resData  as! Dictionary<String, Any>
                            //var arrayData = self.chartPlacesArray[0]["labels"]  as? [String] ?? [""]
                            //var arrayData1 = self.chartPlacesArray[0]["labels"]  as? [String] ?? [""]
                    }
                        self.barChart.clear()
                        self.updateBarChart(barChart: self.barChart)
        }
    }
    
    override public func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          print("FirstViewController will appear")
      }

    override public func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          print("FirstViewController will disappear")
    }
    

   func updateBarChart( barChart  : BarChartView) {
    var barChartEntry  = [ChartDataEntry]()
    var arrayColor : [UIColor] = []
    stackLabels = []
    var productDatasets = self.chartPlacesArray["datasets"] as! [NSDictionary]
                     var countData : Int = 1000
                     var countTotal : Int = productDatasets.count
                     for i in 0..<countTotal {
                         if(countData > (productDatasets[i]["data"]! as AnyObject).count){
                             countData = (productDatasets[i]["data"]! as AnyObject).count
                         }
                         
                     }
             
                       for i in 0..<countData {

                            var dataArrayYaxis : [Double] = []
                            for icounter in 0..<( productDatasets as AnyObject).count {
                                var value = productDatasets[icounter]["data"] as! [Double]
                             if value != nil {
                                 dataArrayYaxis.append( value[i])
                             }
                            }
                           //var  valueData = Double(chartDataArray[i])
                           //let value = BarChartDataEntry(x: Double(i), y: valueData )
                           let value = BarChartDataEntry(x: Double(i), yValues: dataArrayYaxis )
                           barChartEntry.append(value) // here we add it to the data set
                         
                   }
    
             //this is the Array that will eventually be displayed on the graph.
             
             //for i in 0..<dict["data"]!.count {
             
             //var colorsData  : [String] = self.chartPlacesArray[position]["datasets"] as! [String]
           
    
           for cout in 0..<(self.chartPlacesArray["datasets"]! as AnyObject).count {
                   //var color = "#000000"
                   var color : UIColor!
                   //var colorCode = self.chartPlacesArray[position]["colors"][cout]  as? String ?? ""
                   
                   var colorValue = productDatasets[cout]["backgroundColor"]   as? String ?? ""
                   
                   color = UIColor.init(hex: colorValue+"ff")//UIColor("#000000ff")
                   var stackLabel = productDatasets[cout]["label"]   as? String ?? ""
                   stackLabels.append(stackLabel)
                   arrayColor.append(color)
             }
             
                let line1 = BarChartDataSet(entries: barChartEntry, label: self.chartPlacesArray["type"]  as? String ?? "") //Here we convert lineChartEntry to a LineChartDataSet
                 line1.colors = arrayColor
                 line1.stackLabels = stackLabels

                let data = BarChartData(dataSet: line1)//This is the object that will be added to the chart
                //data.barWidth = 0.9
                //data.addDataSet(line1) //Adds the line to the dataSet
                
              
                let xAxis = barChart.xAxis
                xAxis.labelPosition = .bottom
                xAxis.labelFont = .systemFont(ofSize: 10)
                xAxis.granularity = 1
                //xAxis.axisMinimum = 0
                //xAxis.labelCount = 7
               
                User.sharedInstance.xaxisValues  = self.chartPlacesArray["labels"] as! [String]
                print(User.sharedInstance.xaxisValues)
         
         
                //IndexAxisValueFormatter formate = Index
                 xAxis.valueFormatter =  IndexAxisValueFormatter(values: User.sharedInstance.xaxisValues)
                 //DayAxisValueFormatter(chart: barChart)

                barChart.data = data //finally - it adds the chart data to the chart and causes an update
                barChart.chartDescription?.text = "" // Here we set the description for the graph
                barChart.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
         //barChart.xAxis.axisMinimum = 0
         //BarChartDataEntry
          //var chartDataArray  : [Int] = self.chartPlacesArray[position]["datasets"] as! [Int]

   }
    
    var chartIndex : Int = 0
    var vSpinner : UIView?
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
    
}

