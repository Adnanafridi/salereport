//
//  SecondViewController.swift
//  SalesReport
//
//  Created by user on 02/02/2020.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit
import Charts
import Alamofire
import SwiftyJSON
import DropDown

class ProductViewController: UIViewController, UITableViewDelegate, UITableViewDataSource , MyDelegate{
    
    func filterCallBack(year: String, month: String, fromDate: String, toDate: String, location: String, product: String, salesman: String, day: String) {
        apiHandler(yearApi: year,monthApi: month,fromDateApi: fromDate,toDateApi: toDate,locationApi: location,productApi: product)
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var viewLineChart: LineChartView!
    var chartPlacesArray =  [Dictionary<String, Any>]()
    
    @IBOutlet weak var vieBarChart: BarChartView!
    @IBOutlet weak var sortByStack: UIStackView!
    @IBOutlet weak var filterStock: UIStackView!
    @IBOutlet weak var filterLbl: UILabel!
    var arrProducts = [[String:AnyObject]]()
    var productItem = [String:AnyObject]()
    var productDetail = [Any]()
    
    @IBOutlet weak var filterstack: UIStackView!
    
    @IBOutlet weak var ivsort: UIImageView!
    @IBOutlet weak var sortBydropdownIV: UIImageView!
    @IBOutlet weak var sortByLbl: UILabel!
    
    let dropDown = DropDown()
    
    var yearGloble : String = ""
    var monthGloble : String = ""
    var fromDateGloble : String = ""
    var toDateGloble : String = ""
    var locationGloble : String = ""
    var productGloble : String = ""
    
    override open func viewDidLoad()
    {
            super.viewDidLoad()
            
            //updateBarChart()

            tableView.delegate = self
            tableView.dataSource = self
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = UIImage(named:"filter")
        //Set bound to reposition
        let imageOffsetY:CGFloat = 0.0;
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        //Add image to mutable string
        completeText.append(attachmentString)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: "    Filter")
        completeText.append(textAfterIcon)
        self.filterLbl.textAlignment = .center;
        self.filterLbl.attributedText = completeText;

        var color2 = UIColor(netHex:0x2962ff)
        self.sortByStack.addBackground(color: color2)
        //self.sortByStack.spacing = 2
        
        self.filterStock.addBackground(color: color2)
        //self.filterstack.spacing = 2
        apiHandler(yearApi: "",monthApi: "",fromDateApi: "2019-08-01",toDateApi: User.sharedInstance.currentDate(),locationApi: "",productApi: productGloble)
        
        
        
        ivsort.image = UIImage(named:"sort_dec")
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        ivsort.isUserInteractionEnabled = true
        ivsort.addGestureRecognizer(tapGestureRecognizer)
        
        
        let tapGesturesortTapped = UITapGestureRecognizer(target: self, action: #selector(sortTapped(tapGestureRecognizer:)))
        sortBydropdownIV.isUserInteractionEnabled = true
        sortBydropdownIV.addGestureRecognizer(tapGesturesortTapped)
        
        
        let tapGesturesort = UITapGestureRecognizer(target: self, action: #selector(sortTapped(tapGestureRecognizer:)))
        sortByLbl.isUserInteractionEnabled = true
        sortByLbl.addGestureRecognizer(tapGesturesort)
        
        let tapGestureFilter = UITapGestureRecognizer(target: self, action: #selector(filterTapped(tapGestureRecognizer:)))
        filterLbl.isUserInteractionEnabled = true
        filterLbl.addGestureRecognizer(tapGestureFilter)
        dropdownSort()

    }
    
    
    var sortBt : String = "desc"
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        if self.sortByLbl.text == "Sort By"{
            let alertController = UIAlertController(title: "Alert", message:
                "Please select Sort By", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        if sortBt == "desc" {
            sortBt = "asc"
            ivsort.image = UIImage(named:"sort_assending")
        }else {
            sortBt = "desc"
            ivsort.image = UIImage(named:"sort_dec")
        }
        
        self.apiHandler(yearApi: yearGloble,monthApi: monthGloble,fromDateApi: fromDateGloble,toDateApi: toDateGloble,locationApi: locationGloble, productApi: productGloble)
        
    }
    
    
    @objc func filterTapped(tapGestureRecognizer: UITapGestureRecognizer){
        popupFilterCall()
     }
    
    public func popupFilterCall(){
           let vc = storyboard?.instantiateViewController(withIdentifier: "PopupFilterProductController") as? FilterPopUpProduct
            vc?.delegate = self
            vc?.callFrom = "Products"
        vc?.fromDate = fromDateGloble
            vc?.toDate = toDateGloble
        vc?.year = yearGloble
        vc?.month = monthGloble
        vc?.location = locationGloble
        vc?.product = productGloble
        
        self.present(vc!, animated: true, completion: nil)
    }
       
    
    @objc func sortTapped(tapGestureRecognizer: UITapGestureRecognizer){
        dropDown.show()
     }
    
    func dropdownSort() {

        // The view to which the drop down will appear on
        dropDown.anchorView = view // UIView or UIBarButtonItem
        
        dropDown.dataSource = User.sharedInstance.sortBy
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.dropDown.hide()
            if item == "Remove Sort By"{
                self.sortByLbl.text = "Sort By"
            }
            else {
                self.sortByLbl.text = item
            }//
            self.apiHandler(yearApi: self.yearGloble,monthApi: self.monthGloble,fromDateApi: self.fromDateGloble,toDateApi: self.toDateGloble,locationApi: self.locationGloble,productApi: self.productGloble)
        }
        dropDown.bottomOffset = CGPoint(x: (dropDown.anchorView?.plainView.bounds.width)!/10, y:(dropDown.anchorView?.plainView.bounds.width)!/2.7)
        dropDown.width = 200
        dropDown.direction = .any
        DropDown.appearance().cornerRadius = 10
        
    }
    
    
    func apiHandler (yearApi : String , monthApi : String , fromDateApi : String , toDateApi : String , locationApi : String , productApi : String ) {
        
        showSpinner(onView: self.view!)
        
            var yearParam : String = ""
            var monthParam : String = ""
            var fromDateParam : String = ""
            var toDateParam : String = ""
            var locationParam : String = ""
            var productParam : String = ""
            var sortByAss : String = ""
            var sortByDec : String = ""
            
            yearParam = yearApi;
            monthParam=monthApi;
            fromDateParam=fromDateApi;
            toDateParam=toDateApi
            locationParam=locationApi
            productParam=productApi
            
            
            yearGloble = yearApi;
            monthGloble=monthApi;
            fromDateGloble=fromDateApi;
            toDateGloble=toDateApi
            locationGloble=locationApi
            productGloble = productApi
            
            if yearApi != ""{
                yearParam = "'"+yearApi+"'"
            }
            
            if monthApi != ""{
                monthParam = "'"+monthApi+"'"
            }
            
            if fromDateApi != ""{
                fromDateParam = ""+fromDateApi+""
            }
            
            if toDateApi != ""{
                toDateParam = ""+toDateApi+""
            }
            
            if locationApi != ""{
                locationParam = "'"+locationApi+"'"
            }
            
            if productParam != ""{
                productParam = "'"+productApi+"'"
            }
        
            if self.sortByLbl.text == "Sort By"{
                sortByAss = ""
                sortByDec = ""
            } else if self.sortByLbl.text == "Quantity"{
                if sortBt == "asc" {
                    sortByAss = "qty"
                    sortByDec = ""
                }else {
                    
                    sortByAss = ""
                    sortByDec = "qty"
                }
                
            }else {
                if sortBt == "asc" {
                   sortByAss = "revenue"
                   sortByDec = ""
                }else {
                    sortByAss = ""
                    sortByDec = "revenue"
                }
            }
            
            //["Quantity" , "Revenue"]
            
        let parameters: Parameters =    ["year": yearParam,
                                          "month": monthParam,
                                          "fromdate": fromDateParam,
                                          "todate": toDateParam,
                                          "location": locationParam,
                                          "product": productParam,
                                          "sortdesc": sortByDec,
                                          "sortass": sortByAss,
                                          "username": "sshah1"]
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "X-HTTP-Method-Override": "PATCH"
        ]
        
        let URL:String = "http://192.168.18.115:8000/product-mobile-api"
        Alamofire.request(URL, method: .post, parameters: parameters ,encoding: JSONEncoding.default, headers:headers)
                       .validate()
                       .responseJSON { response in
                   self.removeSpinner()
                        guard response.result.isSuccess else {
                        print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                            let alertController = UIAlertController(title: "Alert", message:
                                "No Record Available", preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                            self.present(alertController, animated: true, completion: nil)
                            self.chartPlacesArray =  [Dictionary<String, Any>]()
                            self.tableView.reloadData()
                        return
                    }
                    let swiftyJsonVar = JSON(response.result.value!)
                    if let resData = swiftyJsonVar["loop"].arrayObject {
                        self.chartPlacesArray  = resData  as! [Dictionary<String, Any>]
                        self.tableView.reloadData()
                        //var arrayData = self.chartPlacesArray[0]["labels"]  as? [String] ?? [""]
                        //var arrayData1 = self.chartPlacesArray[0]["labels"]  as? [String] ?? [""]
                    }

            }
    }
    
    override public func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          print("FirstViewController will appear")
    }

    override public func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          print("FirstViewController will disappear")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chartPlacesArray .count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CellBarChart = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! CellBarChart
        let type = self.chartPlacesArray[indexPath.row]["type"] as? String ?? ""
        let name = self.chartPlacesArray[indexPath.row]["itemName"] as? String ?? ""
        cell.lblName.text = name+" "+type
        cell.barchart.clear()
        updateBarChart(position: indexPath.row,barChart: cell.barchart)
        
        return cell
    }
    
    var indexRow : Int = 0
    func updateBarChart( position : Int ,barChart  : BarChartView) {
        do {
            var barChartEntry  = [ChartDataEntry]() //this is the Array that will eventually be displayed on the graph.
            
            //for i in 0..<dict["data"]!.count {
            
            var colorsData  : [String] = self.chartPlacesArray[position]["colors"] as! [String]
            var arrayColor : [UIColor] = []
                for cout in 0..<(self.chartPlacesArray[position]["colors"]! as AnyObject).count {
                    //var color = "#000000"
                    var color : UIColor!
                    
                    //var colorCode = self.chartPlacesArray[position]["colors"][cout]  as? String ?? ""
                    
                    color = UIColor.init(hex: colorsData[cout]+"ff")//UIColor("#000000ff")
                    
                    arrayColor.append(color)
                }
            
               let line1 = BarChartDataSet(entries: barChartEntry, label: self.chartPlacesArray[position]["itemName"]  as? String ?? "") //Here we convert lineChartEntry to a LineChartDataSet
                line1.colors = arrayColor
                line1.label = self.chartPlacesArray[position]["itemName"]  as? String ?? ""
                //line1.drawValuesEnabled = false
                //line1.stackLabels = stackLabels
               //line1.colors = [NSUIColor.blue] //Sets the colour to blue

               let data = BarChartData() //This is the object that will be added to the chart
               data.barWidth = 0.9
               data.addDataSet(line1) //Adds the line to the dataSet
               
             
               let xAxis = barChart.xAxis
               xAxis.labelPosition = .bottom
               xAxis.labelFont = .systemFont(ofSize: 10)
               xAxis.granularity = 1
               //xAxis.axisMinimum = 0
               //xAxis.labelCount = 7
               print(self.chartPlacesArray[position])
               User.sharedInstance.xaxisValues  = self.chartPlacesArray[position]["labels"] as! [String]
               print(User.sharedInstance.xaxisValues)
                
               //IndexAxisValueFormatter formate = Index
               xAxis.valueFormatter =  IndexAxisValueFormatter(values: User.sharedInstance.xaxisValues)
               //DayAxisValueFormatter(chart: barChart)

               barChart.data = data //finally - it adds the chart data to the chart and causes an update
               barChart.chartDescription?.text = "" // Here we set the description for the graph
               barChart.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
                //barChart.xAxis.axisMinimum = 0

                
        //BarChartDataEntry
         var chartDataArray  : [Int] = self.chartPlacesArray[position]["data"] as! [Int]
             for i in 0..<(self.chartPlacesArray[position]["data"]! as AnyObject).count {
                 //let mult = i + 1
                 //var val1 = Double(1 + Double(dict["data"]![i]))
                 //var val2 = Double(10 + i)
                 //var val3 = Double(50 + i)
                 //let value = BarChartDataEntry(x: Double(i), y: numbers[i]) // here we set the X and Y status in a data chart entry
                 //var1 = Double(val1)
                 //val2 = Double(val2)
                 //val3 = Double(val3)
                 //BarChartDataEntry
                 //var barCode = self.chartPlacesArray[position]["data"]  as? String ?? ""
                 var  valueData = Double(chartDataArray[i])
                 let value = BarChartDataEntry(x: Double(i), y: valueData )
                 //let value = BarChartDataEntry(x: Double(i), yValues: [val1, val2, val3] )
                 barChartEntry.append(value) // here we add it to the data set
                 
         }
        var set1: BarChartDataSet! = nil
        set1 = line1
        set1.replaceEntries(barChartEntry)
        //let line2 = barChart.data?.getDataSetByIndex(0)
        //line2?.addEntry( ChartDataEntry )
        barChart.zoom(scaleX: 0, scaleY: 0, x: 0, y: 0)
        barChart.data?.notifyDataChanged()
        barChart.notifyDataSetChanged()
        
        // notify data change (again?)
        // if I comment the following line -> CRASH (again)

        // set the maximum visible range
        // why I do this fixed setup at every value update? Because if I set it at the moment of the chart creation it CRASHs
        barChart.setVisibleXRangeMaximum(50)
        
        // calculate offset
        let offset = max (chartIndex - 50, 0 )

        // CRASH HERE (if I remove all datasets and re-add one or more -> crash)
        barChart.moveViewToX(Double(offset))

        chartIndex += 1
            
        } catch {
            print("The file could not be loaded")
        }
    }
    
      var chartIndex : Int = 0
      var vSpinner : UIView?
      func showSpinner(onView : UIView) {
          let spinnerView = UIView.init(frame: onView.bounds)
          spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
          let ai = UIActivityIndicatorView.init(style: .whiteLarge)
          ai.startAnimating()
          ai.center = spinnerView.center
          
          DispatchQueue.main.async {
              spinnerView.addSubview(ai)
              onView.addSubview(spinnerView)
          }
          
          vSpinner = spinnerView
      }
      
      func removeSpinner() {
          DispatchQueue.main.async {
              self.vSpinner?.removeFromSuperview()
              self.vSpinner = nil
          }
      }
    }


