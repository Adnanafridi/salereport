//
//  DayLocationViewControllerTest.swift
//  SalesReport
//
//  Created by user on 05/02/2020.
//  Copyright © 2020 user. All rights reserved.
//


import UIKit
import Charts
import Alamofire

class DayLocationViewControllerTest: UIViewController {

    @IBOutlet weak var barChart: BarChartView!
    
    @IBOutlet weak var filterByStack: UIStackView!
    var numbers : [Double] = []
    @IBOutlet weak var filterLbl: UILabel!
    
    override open func viewDidLoad()
    {
            super.viewDidLoad()
            self.title = "Day and Location"
            
            numbers.append(10) //here we add the data to the array.
            numbers.append(1)
            numbers.append(5)
            numbers.append(20)
            numbers.append(100)
            numbers.append(10) //here we add the data to the array.
            numbers.append(1)
            numbers.append(5)
            numbers.append(20)
            numbers.append(100)
            numbers.append(20)
            numbers.append(100)
            
        
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = UIImage(named:"filter")
        //Set bound to reposition
        let imageOffsetY:CGFloat = 0.0;
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        //Add image to mutable string
        completeText.append(attachmentString)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: "    Filter")
        completeText.append(textAfterIcon)
        self.filterLbl.textAlignment = .center;
        self.filterLbl.attributedText = completeText;
        
        var color2 = UIColor(netHex:0x2962ff)
        self.filterByStack.addBackground(color: color2)

        apiHandler()
    }
    
    func apiHandler () {
        let parameters: Parameters = ["year": "",
                                      "month": "",
                                      "fromdate": "",
                                      "todate": "",
                                      "location": "",
                                      "sortdesc": "",
                                      "sortass": "",
                                      "username": "sshah1"]
        let URL:String = "http://lp00491:8000/daylocation-mobile-api"
        Alamofire.request(URL, method: .post, parameters: parameters)
                    .validate()
                    .responseJSON { response in
                    // 3 - HTTP response handle
                    guard response.result.isSuccess else {
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    return
                        
                 print(response)
                }
        }
    }
    
    override public func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          print("FirstViewController will appear")
      }

      override public func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          print("FirstViewController will disappear")
      }
    
    var stackLabels: [String] = ["test1","test2","test3"]

    func updateBarChart(barChart : BarChartView){
               var lineChartEntry  = [BarChartDataEntry]() //this is the Array that will eventually be displayed on the graph.
               
               //here is the for loop
               for i in 0..<numbers.count {
                    let mult = i + 1
                    var val1 = Double(1 + i)
                    var val2 = Double(10 + i)
                    var val3 = Double(50 + i)
                    //let value = BarChartDataEntry(x: Double(i), y: numbers[i]) // here we set the X and Y status in a data chart entry
                    //var1 = Double(val1)
                    //val2 = Double(val2)
                    //val3 = Double(val3)

                //BarChartDataEntry
                //let value = BarChartDataEntry(x: Double(i), y: [val1,val2,val3])
                let value = BarChartDataEntry(x: Double(i), yValues: [val1, val2, val3] )
                lineChartEntry.append(value) // here we add it to the data set
               }
    
            var arrayColor : [UIColor] = []
                for cout in 0..<3 {
                    //var color = "#000000"
                    var gold : UIColor!
                    if cout == 0 {
                        gold = UIColor.init(hex: "#000000"+"ff")//UIColor("#000000ff")
                    }else if cout == 1 {
                        gold = UIColor.init(hex: "#FDD7E4"+"ff")//UIColor("#000000ff")
                    }else if cout == 2 {
                        gold = UIColor.init(hex: "#AF9B60"+"ff")//UIColor("#000000ff")
                    }else if cout == 3 {
                        gold = UIColor.init(hex: "#3EA99F"+"ff")//UIColor("#000000ff")
                    }else if cout == 4 {
                        gold = UIColor.init(hex: "#0000FF"+"ff")//UIColor("#000000ff")
                    }else if cout == 5 {
                        gold = UIColor.init(hex: "#0000A0"+"ff")//UIColor("#000000ff")
                    }else {
                        gold = UIColor.init(hex: "#00FFFF"+"ff")//UIColor("#000000ff")
                    }
                        
                    arrayColor.append(gold)
                }
            
               let line1 = BarChartDataSet(entries: lineChartEntry, label: "") //Here we convert lineChartEntry to a LineChartDataSet
                line1.colors = arrayColor
                line1.stackLabels = stackLabels
               //line1.colors = [NSUIColor.blue] //Sets the colour to blue

               let data = BarChartData() //This is the object that will be added to the chart
               data.addDataSet(line1) //Adds the line to the dataSet
               
               let xAxis = barChart.xAxis
               xAxis.labelPosition = .bottom
               xAxis.labelFont = .systemFont(ofSize: 10)
               xAxis.granularity = 1
               xAxis.labelCount = 7
               xAxis.valueFormatter = DayAxisValueFormatter(chart: barChart)

               barChart.data = data //finally - it adds the chart data to the chart and causes an update
               barChart.chartDescription?.text = "" // Here we set the description for the graph
               barChart.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)

           }
        
}

