//
//  ViewControllerTest.swift
//  SalesReport
//
//  Created by user on 06/02/2020.
//  Copyright © 2020 user. All rights reserved.
//

import UIKit
import Charts
import Alamofire
import SwiftyJSON
import DropDown


class ViewControllerTest: UIViewController, UITableViewDelegate, UITableViewDataSource , MyDelegate , UIPopoverPresentationControllerDelegate {
    
    func filterCallBack(year: String, month: String, fromDate fromMonth: String, toDate toMonth: String, location: String, place: String, salesman: String, day: String) {
        print("")
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortByStack: UIStackView!
    
    @IBOutlet weak var filterstack: UIStackView!
    
    @IBOutlet weak var filterLbl: UILabel!
    @IBOutlet weak var ivsort: UIImageView!
    @IBOutlet weak var sortBydropdownIV: UIImageView!
    @IBOutlet weak var sortByLbl: UILabel!
    
    var numbers : [Double] = []
    let dropDown = DropDown()
    
    override open func viewDidLoad()
    {
        super.viewDidLoad()
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = UIImage(named:"filter")
        //Set bound to reposition
        let imageOffsetY:CGFloat = 0.0;
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        //Add image to mutable string
        completeText.append(attachmentString)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: "    Filter")
        completeText.append(textAfterIcon)
        self.filterLbl.textAlignment = .center;
        self.filterLbl.attributedText = completeText;
        
        var color2 = UIColor(netHex:0x2962ff)
        self.sortByStack.addBackground(color: color2)
            //self.sortByStack.spacing = 2
        
        self.filterstack.addBackground(color: color2)
        
            numbers.append(10) //here we add the data to the array.
            numbers.append(1)
            numbers.append(5)
            numbers.append(20)
            numbers.append(100)
            numbers.append(10) //here we add the data to the array.
            numbers.append(1)
            numbers.append(5)
            numbers.append(20)
            numbers.append(100)
            numbers.append(20)
            numbers.append(100)
            
            //updateBarChart()

            tableView.delegate = self
            tableView.dataSource = self
        
        
        ivsort.image = UIImage(named:"sort_dec")
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        ivsort.isUserInteractionEnabled = true
        ivsort.addGestureRecognizer(tapGestureRecognizer)
        
        
        let tapGesturesortTapped = UITapGestureRecognizer(target: self, action: #selector(sortTapped(tapGestureRecognizer:)))
        sortBydropdownIV.isUserInteractionEnabled = true
        sortBydropdownIV.addGestureRecognizer(tapGesturesortTapped)
        
        
        let tapGesturesort = UITapGestureRecognizer(target: self, action: #selector(sortTapped(tapGestureRecognizer:)))
        sortByLbl.isUserInteractionEnabled = true
        sortByLbl.addGestureRecognizer(tapGesturesort)
        
        let tapGestureFilter = UITapGestureRecognizer(target: self, action: #selector(filterTapped(tapGestureRecognizer:)))
        filterLbl.isUserInteractionEnabled = true
        filterLbl.addGestureRecognizer(tapGestureFilter)
        
        //imageName = "sort_dec.png"
        //var imageView : UIImageView
        //imageView  = UIImageView(frame:CGRectMake(10, 50, 100, 300));
        //self.view.addSubview(imageView)
        
        apiHandler()
        dropdownSort()
        apiLocationHandler()
        apiProductHandler()
        apiSaleManHandler()
        //apiSortingHandler()
    }
    
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        print("")
    }

    func DoSomething(text: String) {
           print("")
    }
       
    @objc func filterTapped(tapGestureRecognizer: UITapGestureRecognizer){
        popupFilterCall()
     }
    
    public func popupFilterCall(){
           let vc = storyboard?.instantiateViewController(withIdentifier: "PopupFilterController") as? FilterPopUp
            vc?.delegate = self
            self.present(vc!, animated: true, completion: nil)
    }
       
    
    @objc func sortTapped(tapGestureRecognizer: UITapGestureRecognizer){
        dropDown.show()
     }
    
    func dropdownSort() {
        
        // The view to which the drop down will appear on
        dropDown.anchorView = view // UIView or UIBarButtonItem

        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["Car", "Motorcycle", "Truck"]
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            self.dropDown.hide()
            self.sortByLbl.text = item
        }
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        DropDown.appearance().cornerRadius = 10
        
    }
    
    
    
    func apiHandler () {
        let parameters: Parameters = ["year": "",
                                      "month": "",
                                      "fromdate": "",
                                      "todate": "",
                                      "location": "",
                                      "sortdesc": "",
                                      "sortass": "",
                                      "username": "sshah1"]
        let URL:String = "http://lp00491:8000/summary-mobile-api"
        Alamofire.request(URL, method: .post, parameters: parameters)
                    .validate()
                    .responseJSON { response in
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                        return
                    }
                //let swiftyJsonVar = JSON(response.result.value!)
                //print(swiftyJsonVar)
        }
    }
    
    var sortBt : String = "desc"
    var imageName : String = ""
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        // Your action

        if sortBt == "desc" {
            imageName = "sort_dec.png"
            sortBt = "asc"
            ivsort.image = UIImage(named:"sort_dec")
        }else {
            imageName = "sort_assending.png"
            sortBt = "desc"
            ivsort.image = UIImage(named:"sort_assending")
        }
        //let image = UIImage(named: imageName)
        //let imageView = UIImageView(image: image!)
        //imageView.frame = CGRect(x: 0, y: 0, width: 100, height: 200)
        //ivsort.addSubview(imageView)
    }
    
    override public func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          print("FirstViewController will appear")
    }

    override public func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          print("FirstViewController will disappear")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 10
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CellLineChart = self.tableView.dequeueReusableCell(withIdentifier: "cellproduct") as! CellLineChart

        updateGraph(lineChart: cell.lineChartView)
        
        return cell
    }
    
    func updateGraph(lineChart : LineChartView) {
        
        let data = LineChartData()
        
        for index in 0...2  {
            var lineChartEntry  = [ChartDataEntry]() //this is the Array that will eventually be displayed on the graph.
            
                //here is the for loop
                for i in 0..<numbers.count {
                let convertData = Double(index) * numbers[i]
                   var val1 = Double(convertData)
                   //let value = ChartDataEntryBase(x)
                   let value = ChartDataEntry(x: Double(i), y: val1) // here we set the X and Y status in a data chart entry
                   lineChartEntry.append(value) // here we add it to the data set
               }

               let line1 = LineChartDataSet(entries: lineChartEntry, label: "Number") //Here we convert lineChartEntry to a LineChartDataSet
               line1.colors = ChartColorTemplates.material()

            if index == 0 {
               line1.colors = [NSUIColor.blue] //Sets the colour to blue
            }else if index == 1 {
               line1.colors = [NSUIColor.green] //Sets the colour to blue
            }else if index == 2 {
               line1.colors = [NSUIColor.black] //Sets the colour to blue
            }
                //This is the object that will be added to the chart
               data.addDataSet(line1) //Adds the line to the dataSet
        }

            let xAxis = lineChart.xAxis
            xAxis.labelPosition = .bottom
            xAxis.labelFont = .systemFont(ofSize: 10)
            xAxis.granularity = 1
            xAxis.labelCount = 7
            xAxis.valueFormatter = DayAxisValueFormatter(chart: lineChart)
            
            
            lineChart.data = data //finally - it adds the chart data to the chart and causes an update
            lineChart.chartDescription?.text = "" // Here we set the description for the graph
            lineChart.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.popoverPresentationController?.delegate = self
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
         numbers.append(100)
    }
    
    
    func apiLocationHandler () {

        let URL:String = "http://lp00491:8000/ginvsaleLocationLOV"
        Alamofire.request(URL, method: .get, parameters: nil)
                    .validate()
                    .responseJSON { response in
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                        return
                    }
                    let swiftyJsonVar = JSON(response.result.value!)
                        if let resData = swiftyJsonVar["data"].arrayObject {
                            User.sharedInstance.placesArray  = resData  as! [Dictionary<String, Any>]
                    }
        }
        
    }
    
    
    func apiSortingHandler () {

        let URL:String = "http://lp00491:8000/ginvSortingLOV"
        Alamofire.request(URL, method: .get, parameters: nil)
                    .validate()
                    .responseJSON { response in
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                        return
                    }
                        let swiftyJsonVar = JSON(response.result.value!)
                        if let resData = swiftyJsonVar["data"].arrayObject {
                            User.sharedInstance.placesArray  = resData  as! [Dictionary<String, Any>]
                        }
        }
        
    }
    
    func apiProductHandler () {

        let URL:String = "http://lp00491:8000/ginvProductLOV"
        Alamofire.request(URL, method: .get, parameters: nil)
                    .validate()
                    .responseJSON { response in
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                        return
                    }
                    let swiftyJsonVar = JSON(response.result.value!)
                    if let resData = swiftyJsonVar["data"].arrayObject {
                        User.sharedInstance.placesArray  = resData  as! [Dictionary<String, Any>]
                    }
                        
        }
        
    }
    
    func apiSaleManHandler () {

        let URL:String = "http://lp00491:8000/ginvSaleManLOV"
        Alamofire.request(URL, method: .get, parameters: nil)
                    .validate()
                    .responseJSON { response in
                    guard response.result.isSuccess else {
                        print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                        return
                    }
                        let swiftyJsonVar = JSON(response.result.value!)
                        if let resData = swiftyJsonVar["data"].arrayObject {
                            User.sharedInstance.salesmanArray  = resData  as! [Dictionary<String, Any>]
                        }
        }
        
    }
    

}

// 1: Filter sort
// 2: Api handling
