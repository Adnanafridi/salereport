//
//  TargetViewController.swift
//  SalesReport
//
//  Created by user on 02/02/2020.
//  Copyright © 2020 user. All rights reserved.
//


import UIKit
import Charts
import Alamofire
import SwiftyJSON
import DropDown

class TargetViewController: UIViewController, UITableViewDelegate, UITableViewDataSource , MyDelegate {
    
    func filterCallBack(year: String, month: String, fromDate: String, toDate: String, location: String, product: String, salesman: String, day: String) {
        apiHandler(yearApi: year,monthApi: month,fromDateApi: fromDate,toDateApi: toDate,locationApi: location)
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var viewLineChart: LineChartView!
    @IBOutlet weak var vieBarChart: BarChartView!
    
    var spaceBar : Double = 0.2
    var barSpacing : Double = 0.00
    
    @IBOutlet weak var filterByStack: UIStackView!
    @IBOutlet weak var filterLbl: UILabel!
    var chartPlacesArray =  [Dictionary<String, Any>]()
    let dropDown = DropDown()
    
    var yearGloble : String = ""
    var monthGloble : String = ""
    var fromDateGloble : String = ""
    var toDateGloble : String = ""
    var locationGloble : String = ""
    
    override open func viewDidLoad()
    {
        self.title = "Target"
        super.viewDidLoad()
        
        //updateBarChart()
            
        tableView.delegate = self
        tableView.dataSource = self
            
            
        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = UIImage(named:"filter")
        //Set bound to reposition
        let imageOffsetY:CGFloat = 0.0;
        imageAttachment.bounds = CGRect(x: 0, y: imageOffsetY, width: imageAttachment.image!.size.width, height: imageAttachment.image!.size.height)
        //Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        //Initialize mutable string
        let completeText = NSMutableAttributedString(string: "")
        //Add image to mutable string
        completeText.append(attachmentString)
        //Add your text to mutable string
        let  textAfterIcon = NSMutableAttributedString(string: "    Filter")
        completeText.append(textAfterIcon)
        self.filterLbl.textAlignment = .center;
        self.filterLbl.attributedText = completeText;
        
        
        var color2 = UIColor(netHex:0x2962ff)
        self.filterByStack.addBackground(color: color2)
        //self.filterstack.spacing = 2
        apiHandler(yearApi: "",monthApi: "",fromDateApi: "2019-08-01",toDateApi: User.sharedInstance.currentDate(),locationApi: "")
        
        
        let tapGestureFilter = UITapGestureRecognizer(target: self, action: #selector(filterTapped(tapGestureRecognizer:)))
        filterLbl.isUserInteractionEnabled = true
        filterLbl.addGestureRecognizer(tapGestureFilter)
        
    }

        
    
    @objc func filterTapped(tapGestureRecognizer: UITapGestureRecognizer){
        popupFilterCall()
     }
    
    public func popupFilterCall(){
           let vc = storyboard?.instantiateViewController(withIdentifier: "PopupFilterController") as? FilterPopUp
            vc?.delegate = self
            vc?.callFrom = "target"
        vc?.fromDate = fromDateGloble
        vc?.toDate = toDateGloble
        vc?.year = yearGloble
        vc?.month = monthGloble
        vc?.location = locationGloble
            self.present(vc!, animated: true, completion: nil)
    }
       
    
    func apiHandler (yearApi : String , monthApi : String , fromDateApi : String , toDateApi : String , locationApi : String ) {
        
        showSpinner(onView: self.view!)
        var yearParam : String = ""
        var monthParam : String = ""
        var fromDateParam : String = ""
        var toDateParam : String = ""
        var locationParam : String = ""
        var sortByAss : String = ""
        var sortByDec : String = ""
        
        yearParam = yearApi;
        monthParam=monthApi;
        fromDateParam=fromDateApi;
        toDateParam=toDateApi
        locationParam=locationApi
        
        
        yearGloble = yearApi;
        monthGloble=monthApi;
        fromDateGloble=fromDateApi;
        toDateGloble=toDateApi
        locationGloble=locationApi
        
        
        if yearApi != ""{
            yearParam = "'"+yearApi+"'"
        }
        
        if monthApi != ""{
            monthParam = "'"+monthApi+"'"
        }
        
        if fromDateApi != ""{
            fromDateParam = ""+fromDateApi+""
        }
        
        if toDateApi != ""{
            toDateParam = ""+toDateApi+""
        }
        
        if locationApi != ""{
            locationParam = "'"+locationApi+"'"
        }
        
        let parameters: Parameters = ["year": yearParam,
                                      "month": monthParam,
                                      "fromdate": fromDateParam,
                                      "todate": toDateParam,
                                      "location": locationParam,
                                      "username": "sshah1"]
        
        let headers: HTTPHeaders = [
        "Content-Type": "application/json",
        "X-HTTP-Method-Override": "PATCH"
        ]
        let URL:String = "http://192.168.18.115:8000/target-mobile-api"
        
        Alamofire.request(URL, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers:headers)
                    .validate()
                    .responseJSON { response in
        // 3 - HTTP response handle
                self.removeSpinner()
                guard response.result.isSuccess else {
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    let alertController = UIAlertController(title: "Alert", message:
                        "No Record Available", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
                    self.present(alertController, animated: true, completion: nil)
                    self.chartPlacesArray =  [Dictionary<String, Any>]()
                    self.tableView.reloadData()
                    return
                                                
                }
                        let swiftyJsonVar = JSON(response.result.value!)
                                    if let resData = swiftyJsonVar["loop"].arrayObject {
                                        self.chartPlacesArray  = resData  as! [Dictionary<String, Any>]
                                        self.tableView.reloadData()
                        }
                        
        }
    }
    
    override public func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          print("FirstViewController will appear")
      }

      override public func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          print("FirstViewController will disappear")
      }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chartPlacesArray .count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CellBarChart = self.tableView.dequeueReusableCell(withIdentifier: "cellTarget") as! CellBarChart
        cell.lblName.text = self.chartPlacesArray[indexPath.row]["locname"]  as? String ?? ""
        cell.barchart.clear()
        updateBarChart(position: indexPath.row, barChartView: cell.barchart)
        
        return cell
    }
    
    var stackLabels: [String] = [""]

    func updateBarChart(position : Int , barChartView : BarChartView){
            
        // Do any additional setup after loading the view, typically from a nib.
                   //barChartView.delegate = self
                   barChartView.noDataText = "You need to provide data for the chart."
                   barChartView.chartDescription?.text = ""


                   //legend
                   let legend = barChartView.legend
                   legend.enabled = true
                   legend.horizontalAlignment = .right
                   legend.verticalAlignment = .top
                   legend.orientation = .vertical
                   legend.drawInside = true
                   legend.yOffset = 10.0;
                   legend.xOffset = 10.0;
                   legend.yEntrySpace = 0.0;


                   let xaxis = barChartView.xAxis
                   xaxis.drawGridLinesEnabled = true
                   xaxis.labelPosition = .bottom
                   xaxis.centerAxisLabelsEnabled = true
                    User.sharedInstance.xaxisValues  = self.chartPlacesArray[position]["labels"] as! [String]
                    print(User.sharedInstance.xaxisValues)
                    
                   xaxis.valueFormatter = IndexAxisValueFormatter(values:User.sharedInstance.xaxisValues)
                   xaxis.granularity = 1


                   let leftAxisFormatter = NumberFormatter()
                   leftAxisFormatter.maximumFractionDigits = 1

                   let yaxis = barChartView.leftAxis
                   yaxis.spaceTop = 0.35
                   yaxis.axisMinimum = 0
                   yaxis.drawGridLinesEnabled = false

                   barChartView.rightAxis.enabled = false
                  //axisFormatDelegate = self

                  setChart(position: position, barChartView: barChartView)
    }

    func setChart(position : Int ,barChartView : BarChartView) {
        var productDatasets = self.chartPlacesArray[position]["datasets"] as! [NSDictionary]
               barChartView.noDataText = "You need to provide data for the chart."
               var dataEntries: [BarChartDataEntry] = []
               var dataEntries1: [BarChartDataEntry] = []

               for i in 0..<(productDatasets[0]["data"]! as AnyObject).count{
                    
                   var dataEntryFirstString =  productDatasets[0]["data"] as! [Double]
                   let dataEntry = BarChartDataEntry(x: Double(i) , y: dataEntryFirstString[i])
                   dataEntries.append(dataEntry)
                    
                   var dataEntrySecondString =  productDatasets[1]["data"] as! [Double]
                   let dataEntry1 = BarChartDataEntry(x: Double(i) , y: dataEntrySecondString[i])
                   dataEntries1.append(dataEntry1)

                   //stack barchart
                   //let dataEntry = BarChartDataEntry(x: Double(i), yValues:  [self.unitsSold[i],self.unitsBought[i]], label: "groupChart")
               }
        
            //var colorsData  : [String] = self.chartPlacesArray[position]["colors"] as! [String]
            var arrayColor : [UIColor] = []

            var color1 : UIColor!
            var color2 : UIColor!
            
            var colorCode1 = productDatasets[0]["backgroundColor"]   as? String ?? ""
            
            color1 = UIColor.init(hex: colorCode1+"ff")//UIColor("#000000ff")
            
            //arrayColor.append(color1)
        
            var colorCode2 = productDatasets[1]["backgroundColor"]   as? String ?? ""
                    
            color2 = UIColor.init(hex: colorCode2+"ff")//UIColor("#000000ff")
                    
            arrayColor.append(color2)
        
             let chartDataSet = BarChartDataSet(entries: dataEntries, label: productDatasets[0]["label"] as? String ?? "")
            //chartDataSet.color(atIndex: UIColor.init(hex: colorCode1+"ff"))     //atIndex: UIColor.init(hex: colorCode1+"ff"))
            
            let chartDataSet1 = BarChartDataSet(entries: dataEntries1, label: productDatasets[1]["label"] as? String ?? "")
                
            let dataSets: [BarChartDataSet] = [chartDataSet,chartDataSet1]
               chartDataSet.colors = arrayColor
               //chartDataSet.colors = ChartColorTemplates.colorful()
               //let chartData = BarChartData(dataSet: chartDataSet)

               let chartData = BarChartData(dataSets: dataSets)
                

               let groupSpace = 0.3
               let barSpace = 0.05
               let barWidth = 0.3
               // (0.3 + 0.05) * 2 + 0.3 = 1.00 -> interval per "group"

               let groupCount = User.sharedInstance.xaxisValues.count
               let startYear = 0
                
               chartData.barWidth = barWidth;
               barChartView.xAxis.axisMinimum = Double(startYear)
               let gg = chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
               //print("Groupspace: \(gg)")
               barChartView.xAxis.axisMaximum = Double(startYear) + gg * Double(groupCount)

               chartData.groupBars(fromX: Double(startYear), groupSpace: groupSpace, barSpace: barSpace)
               //chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
               //barChartView.notifyDataSetChanged()

               barChartView.data = chartData

               //background color
               barChartView.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)

               //chart animation
               //barChartView.animate(xAxisDuration: 1.5, yAxisDuration: 1.5, easingOption: .linear)

    }

        var vSpinner : UIView?
        func showSpinner(onView : UIView) {
            let spinnerView = UIView.init(frame: onView.bounds)
            spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
            let ai = UIActivityIndicatorView.init(style: .whiteLarge)
            ai.startAnimating()
            ai.center = spinnerView.center
            
            DispatchQueue.main.async {
                spinnerView.addSubview(ai)
                onView.addSubview(spinnerView)
            }
            
            vSpinner = spinnerView
        }
        
        func removeSpinner() {
            DispatchQueue.main.async {
                self.vSpinner?.removeFromSuperview()
                self.vSpinner = nil
            }
        }
}

