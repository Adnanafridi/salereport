//
//  FirstViewController.swift
//  SalesReport
//
//  Created by user on 02/02/2020.
//  Copyright © 2020 user. All rights reserved.
//


import UIKit

public class FirstViewController: UIViewController {

    override public func viewDidLoad() {
        super.viewDidLoad()
    }

    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("FirstViewController will appear")
    }

    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("FirstViewController will disappear")
    }
}
