//
//  user.swift
//  SalesReport
//
//  Created by user on 04/02/2020.
//  Copyright © 2020 user. All rights reserved.
//


import Foundation
class User: NSObject {
  static let sharedInstance = User()

   
    var yearArray =  [Dictionary<String, Any>]()
   var placesArray =  [Dictionary<String, Any>]()
   var salesmanArray =  [Dictionary<String, Any>]()
   var sortArray =  [Dictionary<String, Any>]()
   var locationArray =  [Dictionary<String, Any>]()
   
   var productData : [String] = []
   var locationaData : [String] = []
   var salesmanData : [String] = []
    var yearData : [String] = []
   var xaxisValues : [String] = ["JAN", "FEB", "MAR",
                  "APR", "MAY", "Jun",
                 "Jul", "Aug", "Sep",
                  "Oct", "Nov", "Dec"];
    var dayData : [String] = ["1", "2", "3",
     "4", "5", "6",
    "7", "8", "9",
     "10", "11", "12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"];
    
    var sortBy : [String] = [ "Quantity" , "Revenue", "Remove Sort By"]
    
    
    func currentDate()-> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
}
