//
//  FilterPopUp.swift
//  SalesReport
//
//  Created by user on 04/02/2020.
//  Copyright © 2020 user. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import DropDown


class FilterPopUpProduct: UIViewController  {
    
    var delegate : MyDelegate?
    var callFrom : String = ""
    @IBOutlet weak var popupview: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var viewPredefine: UIView!
    @IBOutlet weak var viewYear: UIView!
    @IBOutlet weak var viewMonth: UIView!
    @IBOutlet weak var viewFromDate: UIView!
    @IBOutlet weak var viewToDate: UIView!
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var viewProducts: UIView!
    
    
    let dropDownPredefine = DropDown()
    let dropDownYear = DropDown()
    let dropDownMonth = DropDown()
    let dropDownLocation = DropDown()
    let dropDownProduct = DropDown()
    
    var fromDate : String = ""
    var toDate : String = ""
    var location : String = ""
    var year : String = ""
    var month : String = ""
    var product : String = ""
    
    @IBOutlet weak var lblPredefine: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblProduct: UILabel!
    
    
    @IBOutlet weak var tfFromDate: UITextField!
    @IBOutlet weak var tfToDate: UITextField!
    var datePickerFrom = UIDatePicker()
    var datePickerTo = UIDatePicker()
    
    @IBOutlet weak var ivPredefineCancel: UIImageView!
    @IBOutlet weak var ivYearCancel: UIImageView!
    @IBOutlet weak var ivMonthCancel: UIImageView!
    @IBOutlet weak var ivFromDateCancel: UIImageView!
    @IBOutlet weak var ivToDateCancel: UIImageView!
    @IBOutlet weak var ivLocationCancel: UIImageView!
    
    @IBOutlet weak var ivProductCancel: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.popupview.layer.cornerRadius = 15
        self.popupview.clipsToBounds = true
        self.popupview.layer.borderColor = UIColor.gray.cgColor
        self.popupview.layer.borderWidth = 1
        
        initField()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.popoverPresentationController?.delegate = self as! UIPopoverPresentationControllerDelegate
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //var delegate:GetTextDelegate?
    
    @IBAction func btnSubmit(_ sender: Any) {
        //User.sharedInstance.userName = lblName.text!
        //if let delegate = self.delegate {
        //DoSomething("newText.text")
        var yearst = lblYear.text  as? String ?? "" //yearst    String    "Select Year"
        var monthsst = lblMonth.text as? String ?? "" //monthsst    String    "Select Month"
        var fromDate = tfFromDate.text as? String ?? "" // fromDate    String    "From Date"
        var toDates = tfToDate.text as? String ?? "" //toDates    String    "To Date"
        var locationLbl = lblLocation.text as? String ?? "" // locationLbl    String    "Select Location"
        var productLbl = lblProduct.text as? String ?? ""
        
        if yearst == "Select Year" {
            yearst = ""
        }
        
        if monthsst == "Select Month" {
            monthsst = ""
        }
        
        if fromDate == "From Date" {
            fromDate = ""
        }
        
        if toDates == "To Date" {
            toDates = ""
        }
        
        if locationLbl == "Select Location" {
            locationLbl = ""
        }
        
        if productLbl == "Select Product" {
            productLbl = ""
        }
        
        delegate!.filterCallBack(year: yearst ,month: monthsst,fromDate: fromDate,toDate: toDates,location: locationLbl,product: productLbl,salesman: "",day: "")
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func initField()   {
        
        dropdownPreDefine()
        dropdownYear()
        dropdownMonth()
        dropdownLocation()
        dropdownProduct()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(preDefineTapped(tapGestureRecognizer:)))
        lblPredefine.isUserInteractionEnabled = true
        lblPredefine.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizerYear = UITapGestureRecognizer(target: self, action: #selector(yearTapped(tapGestureRecognizer:)))
        viewYear.isUserInteractionEnabled = true
        viewYear.addGestureRecognizer(tapGestureRecognizerYear)
        
        let tapGestureRecognizerMonth = UITapGestureRecognizer(target: self, action: #selector(monthTapped(tapGestureRecognizer:)))
        viewMonth.isUserInteractionEnabled = true
        viewMonth.addGestureRecognizer(tapGestureRecognizerMonth)
        
        let tapGestureRecognizerLocation = UITapGestureRecognizer(target: self, action: #selector(locationTapped(tapGestureRecognizer:)))
        lblLocation.isUserInteractionEnabled = true
        lblLocation.addGestureRecognizer(tapGestureRecognizerLocation)
        
        let tapGestureRecognizerPlaces = UITapGestureRecognizer(target: self, action: #selector(placesTapped(tapGestureRecognizer:)))
        viewProducts.isUserInteractionEnabled = true
        viewProducts.addGestureRecognizer(tapGestureRecognizerPlaces)
        
        viewAddBorder(view: self.viewPredefine)
        viewAddBorder(view: self.viewYear)
        viewAddBorder(view: self.viewMonth)
        viewAddBorder(view: self.viewFromDate)
        viewAddBorder(view: self.viewToDate)
        viewAddBorder(view: self.viewLocation)
        viewAddBorder(view: self.viewProducts)
        
        fromDateTextField()
        toDateTextField()
        
        initCancelRecord()
        
        
        if fromDate != "" {
            tfFromDate.text = fromDate
            var dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: fromDate)
            datePickerFrom.setDate(date!, animated: true)
        }
        
        if toDate != "" {
            tfToDate.text = toDate
            var dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: toDate)
            datePickerTo.setDate(date!, animated: true)
        }
        
        if location != "" {
            lblLocation.text = location
        }
        
        if year != "" {
            lblYear.text = year
        }
        
        if month != "" {
            lblMonth.text = month
        }
        if product != "" {
            lblProduct.text = product
        }
        
    }
           
           func initCancelRecord() {

               let tapGestureLocationCancel = UITapGestureRecognizer(target: self, action: #selector(locationCancelTapped(tapGestureRecognizer:)))
               ivLocationCancel.isUserInteractionEnabled = true
               ivLocationCancel.addGestureRecognizer(tapGestureLocationCancel)
               
               let tapGestureToDateCancel = UITapGestureRecognizer(target: self, action: #selector(toDateCancelTapped(tapGestureRecognizer:)))
               ivToDateCancel.isUserInteractionEnabled = true
               ivToDateCancel.addGestureRecognizer(tapGestureToDateCancel)
               
               let tapGestureFromDateCancel = UITapGestureRecognizer(target: self, action: #selector(fromDateCancelTapped(tapGestureRecognizer:)))
               ivFromDateCancel.isUserInteractionEnabled = true
               ivFromDateCancel.addGestureRecognizer(tapGestureFromDateCancel)
               
               
               let tapGestureYearCancel = UITapGestureRecognizer(target: self, action: #selector(yearCancelTapped(tapGestureRecognizer:)))
               ivYearCancel.isUserInteractionEnabled = true
               ivYearCancel.addGestureRecognizer(tapGestureYearCancel)
               
               let tapGestureMonthCancel = UITapGestureRecognizer(target: self, action: #selector(monthCancelTapped(tapGestureRecognizer:)))
               ivMonthCancel.isUserInteractionEnabled = true
               ivMonthCancel.addGestureRecognizer(tapGestureMonthCancel)
               
               let tapGesturePredefineCancel = UITapGestureRecognizer(target: self, action: #selector(predefineCancelTapped(tapGestureRecognizer:)))
               ivPredefineCancel.isUserInteractionEnabled = true
               ivPredefineCancel.addGestureRecognizer(tapGesturePredefineCancel)
               
            
            let tapGestureProductCancel = UITapGestureRecognizer(target: self, action: #selector(productCancelTapped(tapGestureRecognizer:)))
            ivProductCancel.isUserInteractionEnabled = true
            ivProductCancel.addGestureRecognizer(tapGestureProductCancel)
           }
           
            @objc func productCancelTapped(tapGestureRecognizer: UITapGestureRecognizer) {
                lblProduct.text = "Select Product"
            }
    
    
           @objc func locationCancelTapped(tapGestureRecognizer: UITapGestureRecognizer) {
               lblLocation.text = "Select Location"
           }
           
           @objc func toDateCancelTapped(tapGestureRecognizer: UITapGestureRecognizer){
               tfToDate.text = "To Date"
           }
           
           @objc func fromDateCancelTapped(tapGestureRecognizer: UITapGestureRecognizer){
               tfFromDate.text = "From Date"
           }
           
           @objc func yearCancelTapped(tapGestureRecognizer: UITapGestureRecognizer){
               lblYear.text = "Select Year"
           }
           
           @objc func monthCancelTapped(tapGestureRecognizer: UITapGestureRecognizer){
               lblMonth.text = "Select Month"
            }
           
           @objc func predefineCancelTapped(tapGestureRecognizer: UITapGestureRecognizer){
               lblPredefine.text = "Predefine Data Filter"
            }
    
    func fromDateTextField() {
        tfFromDate.inputView = datePickerFrom
        datePickerFrom.datePickerMode = .date
        
        
        //let dateString = "2015-11-20"
        //var dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "yyyy-MM-dd"
        //let date = dateFormatter.date(from: dateString)
        //datePickerFrom.setDate(date!, animated: true)
        
        
        datePickerFrom.addTarget(self, action: #selector(self.datePickerValueChangedfromDate(datePicker:)), for: .valueChanged)
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let todayButton = UIBarButtonItem(title: "Today", style: .plain, target: self, action: #selector(todayfromDatePressed(sender:)))
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(donefromDatePressed(sender:)))
        
        toolbar.setItems([doneButton,todayButton], animated: false)
        tfFromDate.inputAccessoryView = toolbar
        

    }
    
    
    func toDateTextField() {
        tfToDate.inputView = datePickerTo
        datePickerTo.datePickerMode = .date
        datePickerTo.addTarget(self, action: #selector(self.datePickerValueChangedToDate(datePicker:)), for: .valueChanged)
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let todayButton = UIBarButtonItem(title: "Today", style: .plain, target: self, action: #selector(todayToDatePressed(sender:)))
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneToDatePressed(sender:)))
        
        toolbar.setItems([doneButton,todayButton], animated: false)
         tfToDate.inputAccessoryView = toolbar
    }
    
       
       
    @objc func todayfromDatePressed(sender: UIBarButtonItem) {
           let dateFormatter = DateFormatter()
           dateFormatter.dateStyle = .medium
           dateFormatter.timeStyle = .none
           
           tfFromDate.text = dateFormatter.string(from: Date())
           
           tfFromDate.resignFirstResponder()
    }
       
    
    @objc func donefromDatePressed(sender: UIBarButtonItem) {
           tfFromDate.resignFirstResponder()
    }
       
    @objc func datePickerValueChangedfromDate(datePicker: UIDatePicker) {
           let dateFormatter = DateFormatter()
            dateFormatter.dateFormat  = "yyyy-MM-dd"
            //dateFormatter.setDate
            //dateFormatter; setDateFormat:@"MM'/'dd'/'yyyy"];
           //dateFormatter.dateStyle = .medium
           //dateFormatter.timeStyle = .none
           
           tfFromDate.text = dateFormatter.string(from: datePicker.date)
    }
    
    
    @objc func todayToDatePressed(sender: UIBarButtonItem) {
              let dateFormatter = DateFormatter()
                dateFormatter.dateFormat  = "yyyy-MM-dd"
              //dateFormatter.dateStyle = .medium
              //dateFormatter.timeStyle = .none
              
              tfToDate.text = dateFormatter.string(from: Date())
              tfToDate.resignFirstResponder()
    }
          
    
    @objc func doneToDatePressed(sender: UIBarButtonItem) {
              tfToDate.resignFirstResponder()
    }
          
    @objc func datePickerValueChangedToDate(datePicker: UIDatePicker) {
              let dateFormatter = DateFormatter()
               dateFormatter.dateFormat  = "yyyy-MM-dd"
               //dateFormatter.setDate
           //dateFormatter; setDateFormat:@"MM'/'dd'/'yyyy"];
              //dateFormatter.dateStyle = .medium
              //dateFormatter.timeStyle = .none
              
              tfToDate.text = dateFormatter.string(from: datePicker.date)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           view.endEditing(true)
    }
    
    
    @objc func preDefineTapped(tapGestureRecognizer: UITapGestureRecognizer){
        dropDownPredefine.show()
     }
    
    @objc func yearTapped(tapGestureRecognizer: UITapGestureRecognizer){
        dropDownYear.show()
     }
    
    @objc func monthTapped(tapGestureRecognizer: UITapGestureRecognizer){
        dropDownMonth.show()
     }
    
    @objc func locationTapped(tapGestureRecognizer: UITapGestureRecognizer){
        dropDownLocation.show()
     }
    
    @objc func placesTapped(tapGestureRecognizer: UITapGestureRecognizer){
       dropDownProduct.show()
    }
    
    func viewAddBorder(view : UIView){
        
        view.layer.borderWidth = 1
        view.layer.masksToBounds = true
        view.layer.borderColor = UIColor.black.cgColor

        view.layer.borderColor = UIColor(red:222/255, green:225/255, blue:227/255, alpha: 1).cgColor

        view.layer.cornerRadius = 8
        view.clipsToBounds = true
        view.layer.borderColor = UIColor.gray.cgColor
        view.layer.borderWidth = 1
    }
    
    func dropdownPreDefine() {
           
           // The view to which the drop down will appear on
           dropDownPredefine.anchorView = view // UIView or UIBarButtonItem
           
           // The list of items to display. Can be changed dynamically
           dropDownPredefine.dataSource = ["Today", "Last 1 Week", "Last 1 Month","Last 3 Month","Last 6 Month","Last 1 Year"]
           dropDownPredefine.selectionAction = { [unowned self] (index: Int, item: String) in
             print("Selected item: \(item) at index: \(index)")
            self.dropDownPredefine.hide()
            self.lblPredefine.text = item
            
            var lastDate = Calendar.current.date(byAdding: .day, value: 0, to: Date())!
            if item == "Today"{
                lastDate = Calendar.current.date(byAdding: .day, value: 0, to: Date())!
            }else if item == "Last 1 Week"{
                lastDate = Calendar.current.date(byAdding: .weekOfYear, value: -1, to: Date())!
            }else if item == "Last 1 Month"{
                lastDate = Calendar.current.date(byAdding: .month, value: -1, to: Date())!
            }else if item == "Last 3 Month"{
                lastDate = Calendar.current.date(byAdding: .month, value: -3, to: Date())!
            }else if item == "Last 6 Month"{
                lastDate = Calendar.current.date(byAdding: .month, value: -6, to: Date())!
            }else if item == "Last 1 Year"{
                lastDate = Calendar.current.date(byAdding: .year, value: -1, to: Date())!
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            self.tfFromDate.text = dateFormatter.string(from: lastDate)
            self.datePickerFrom.setDate(lastDate, animated: true)
            
            self.tfToDate.text = User.sharedInstance.currentDate()
           }
           dropDownPredefine.bottomOffset = CGPoint(x: 0, y:(dropDownPredefine.anchorView?.plainView.bounds.height)!)
           DropDown.appearance().cornerRadius = 10
           
    }
    
    func dropdownYear() {
           
           // The view to which the drop down will appear on
           dropDownYear.anchorView = view // UIView or UIBarButtonItem
            
           // The list of items to display. Can be changed dynamically
           dropDownYear.dataSource = User.sharedInstance.yearData
           dropDownYear.selectionAction = { [unowned self] (index: Int, item: String) in
             print("Selected item: \(item) at index: \(index)")
               self.dropDownYear.hide()
               self.lblYear.text = item
           }
           dropDownYear.bottomOffset = CGPoint(x: 0, y:(dropDownYear.anchorView?.plainView.bounds.height)!)
           DropDown.appearance().cornerRadius = 10
           
    }
    
    func dropdownMonth() {
           
           // The view to which the drop down will appear on
           dropDownMonth.anchorView = view // UIView or UIBarButtonItem
            
           // The list of items to display. Can be changed dynamically
           dropDownMonth.dataSource = ["JAN", "FEB", "MAR"
                                     ,"APR","MAY","JUN","JUL"
                                    ,"AUG","SEP","OCT","NOV","DEC"]
           dropDownMonth.selectionAction = { [unowned self] (index: Int, item: String) in
             print("Selected item: \(item) at index: \(index)")
               self.dropDownMonth.hide()
               self.lblMonth.text = item
           }
           dropDownMonth.bottomOffset = CGPoint(x: 0, y:(dropDownMonth.anchorView?.plainView.bounds.height)!)
           DropDown.appearance().cornerRadius = 10
           
    }
    
    func dropdownLocation() {
           
           // The view to which the drop down will appear on
           dropDownLocation.anchorView = view // UIView or UIBarButtonItem
           
           // The list of items to display. Can be changed dynamically
           dropDownLocation.dataSource = User.sharedInstance.locationaData
           dropDownLocation.selectionAction = { [unowned self] (index: Int, item: String) in
             print("Selected item: \(item) at index: \(index)")
               self.dropDownLocation.hide()
               self.lblLocation.text = item
           }
           dropDownLocation.bottomOffset = CGPoint(x: 0, y:(dropDownLocation.anchorView?.plainView.bounds.height)!)
           DropDown.appearance().cornerRadius = 10
           
    }
    
    func dropdownProduct() {
           
           // The view to which the drop down will appear on
           dropDownProduct.anchorView = view // UIView or UIBarButtonItem
           
           // The list of items to display. Can be changed dynamically
           dropDownProduct.dataSource = User.sharedInstance.productData
           dropDownProduct.selectionAction = { [unowned self] (index: Int, item: String) in
             print("Selected item: \(item) at index: \(index)")
               self.dropDownProduct.hide()
               self.lblProduct.text = item
           }
           dropDownProduct.bottomOffset = CGPoint(x: 0, y:(dropDownProduct.anchorView?.plainView.bounds.height)!)
           DropDown.appearance().cornerRadius = 10
           
    }
    
}

extension UIDatePicker {

   func setDate(from string: String, format: String, animated: Bool = true) {

      let formater = DateFormatter()

      formater.dateFormat = format

      let date = formater.date(from: string) ?? Date()

      setDate(date, animated: animated)
   }
}
